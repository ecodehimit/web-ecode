<?php

namespace App\Models;

use App\Models\Webinar;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Speaker extends Model
{
    use HasFactory;

    public function webinar()
    {
        return $this->belongsToMany(Webinar::class);
    }
}
