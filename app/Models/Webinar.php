<?php

namespace App\Models;

use App\Models\Speaker;
use App\Models\EcodeMember;
use App\Models\Participant;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Webinar extends Model
{
    use HasFactory;

    public function ecode_members()
    {
        return $this->belongsTo(EcodeMember::class);
    }

    public function participants()
    {
        return $this->hasMany(Participant::class);
    }

    public function speakers()
    {
        return $this->belongsToMany(Speaker::class);
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public static function getWithAllStatus()
    {
        return DB::table('webinars')
            ->leftJoin('ecode_members', 'ecode_members.id', '=', 'webinars.host_id')
            ->select('webinars.*', 'ecode_members.name')
            ->get();
    }
}
