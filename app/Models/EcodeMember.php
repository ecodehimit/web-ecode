<?php

namespace App\Models;

use App\Models\Webinar;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class EcodeMember extends Model
{
    use HasFactory;
    protected $fillable = [
        "user_id",
    ];

    public static function user()
    {
        return session("auth_member") ? EcodeMember::find(\session("auth_member")->id) : \null;
    }

    public static function token()
    {
        return \session("token_member");
    }

    public function ecode_members()
    {
        return $this->hasMany(Webinar::class);
    }

    public static function createIfNotExist(array $member)
    {
        $checkIfMemberWasExisted = EcodeMember::where($member)->first();
        if(!$checkIfMemberWasExisted) {
            $newMember = new EcodeMember();
            $newMember->user_id = $member[0][2];
            $newMember->name = $member[1][2];
            $newMember->college_year = $member[2][2];
            $newMember->save();
            return $newMember;
        } else {
            $checkIfMemberWasExisted->user_id = $member[0][2];
            $checkIfMemberWasExisted->name = $member[1][2];
            $checkIfMemberWasExisted->college_year = $member[2][2];
            $checkIfMemberWasExisted->update();
        }
        return $checkIfMemberWasExisted;
    }
}
