<?php

namespace App\Http\Middleware;

use App\Models\EcodeMember;
use Closure;
use Illuminate\Http\Request;

class LoginCheckMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $isLogined = EcodeMember::user();
        if(!$isLogined) {
            return redirect()->route("login.page")->with("error","silahkan login dulu yah :')");
        }
        return $next($request);
    }
}
