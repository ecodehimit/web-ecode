<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EcodeMember;

class ActiveMemberController extends Controller
{
    public function index()
    {
        $activeMembers = EcodeMember::where("status", true)->get();
        return view("active-member.index", [
            "activeMembers" => $activeMembers,
        ]);
    }

    public function getData(Request $request)
    {
        $user_id = $request->get("user_id") ?? null;
        if (!is_null($user_id)) {
            try {
                $data = EcodeMember::where("user_id", $user_id)->firstOrFail();
                return response()->json([
                    "status" => true,
                    "data" => $data
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    "status" => false,
                    "message" => "Data tidak ditemukan"
                ]);
            }
        }

        try {
            $data = EcodeMember::where("status", true)->get();
            return response()->json([
                "status" => true,
                "data" => $data
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "status" => false,
                "message" => "Data tidak ditemukan"
            ]);
        }
    }

    public function createOrUpdate()
    {
        $allMembers = EcodeMember::all();
        return view("active-member.form", [
            "allMembers" => $allMembers
        ]);
    }

    public function createOrUpdatePost(Request $request)
    {
        $data = $request->validate([
            "user_id" => "required|exists:ecode_members,user_id",
            "college_year" => "required|numeric",
            "position" => "required",
            "github" => "required",
            "instagram" => "required",
            "linkedin_url" => "required",
            "status" => "required|in:1,0",
            "image" => "nullable|image|mimes:jpeg,png,jpg|max:2048",
        ]);

        try {
            $member = EcodeMember::where("user_id", $data["user_id"])->firstOrFail();
            $member->college_year = $data["college_year"];
            $member->position = $data["position"];
            $member->github = $data["github"];
            $member->instagram = $data["instagram"];
            $member->linkedin_url = $data["linkedin_url"];
            $member->status = $data["status"];
            if ($request->hasFile("image")) {
                $image = $request->file("image");
                $imageName = time() . "." . $image->extension();
                $image->move(public_path("images"), $imageName);
                $member->image = $imageName;
            }
            $member->update();
            return response()->json([
                "status" => true,
                "message" => "Berhasil mengubah data"
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "status" => false,
                "message" => $e->getMessage()
            ], 500);
        }
    }
}
