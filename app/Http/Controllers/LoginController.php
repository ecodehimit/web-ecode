<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Models\EcodeMember;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class LoginController extends Controller
{

    public function __construct()
    {
        $this->apiLogin = env('API_ONEGATE')."/api/v1/ecode/auth/login";
    }

    public function loginPage()
    {
        return view("login.login");
    }

    public function loginPost(LoginRequest $request)
    {
        $data = $request->validated();
        
        try {
            $loginToOnegate = json_decode(Http::post($this->apiLogin, $data));
        } catch (\Throwable $th) {
            return \redirect()
            ->back()
            ->with("error", "Mohon tunggu sebentar, server sedang sibuk")
            ->withInput();
        }
        
        if(!$loginToOnegate->status) {
            return \redirect()
                    ->back()
                    ->with("error", $loginToOnegate->errors)
                    ->withInput();
        }


        $ecodeMember = EcodeMember::createIfNotExist([
                            ["user_id", "=", $loginToOnegate->user->id],
                            ["name", "=", $loginToOnegate->user->name],
                            ["college_year", "=", $loginToOnegate->user->has_angkatan->year],
                        ]);

        \session(["auth_member" => $ecodeMember]);
        \session(["token_member" => $loginToOnegate->token]);
        return redirect()->route("dashboard");
    }
}
