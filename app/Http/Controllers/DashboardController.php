<?php

namespace App\Http\Controllers;

use App\Models\EcodeMember;
use App\Models\Webinar;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function dashboardPage()
    {
        $webinars = Webinar::getWithAllStatus();
        // var_dump($webinars); die;
        return view("dashboard.index", compact('webinars'));
    }

    public function logout()
    {
        session()->flush();
        return \redirect()->route("login.page");
    }
}
