<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PhpParser\Builder\Param;
use \App\Models\Webinar;

class LandingPageController extends Controller
{
    public function landingPage()
    {
        $data_path = [
            [
                "title" => "UI/UX Designer Roadmap",
                "description" => "Panduan menjadi UI/UX designer",
                "thumbnail" => asset('assets/alur-belajar/ui-ux.png'),
                "tech" => ["notion", "behance", "figma", "analytic"],
            ],
            [
                "title" => "Backend Web Roadmap",
                "description" => "Panduan menjadi backend web 2023",
                "thumbnail" => asset('assets/alur-belajar/backend.png'),
                "tech" => ["github", "xampp", "postman", "laravel"]
            ],
            [
                "title" => "Frontend Web Roadmap",
                "description" => "Panduan menjadi frontend web 2023",
                "thumbnail" => asset('assets/alur-belajar/frontend.png'),
                "tech" => ["github", "html5", "tailwind", "react"]
            ],
        ];

        $data_testimonial = [
            [
                "user" => "Sam Rakha Putra",
                "major" => "Teknik Informatika",
                "photo" => asset('assets/testimonial/profile-1.png'),
                "message" => "E-Code Web is an incredible resource for programmers of all levels. Whenever I encounter a coding problem or need clarification on a certain topic, E-Code Web is my go-to site. The community is full of knowledgeable and helpful individuals who are always eager to share their expertise and insights. I have learned so much from the countless answers, discussions, and tutorials on the site.",
            ],
            [
                "user" => "Giorgina Rodriguez",
                "major" => "Sains Data Terapan",
                "photo" => asset('assets/testimonial/profile-2.png'),
                "message" => "The available roadmaps are highly knowledgeable and engaging which make the learning experience enjoyable and effective. The platform's interface is user-friendly and intuitive, making it easy to navigate and keep track of my progress. I love the flexibility of being able to learn at my own pace and on my own schedule, which allows me to balance my other responsibilities.",
            ],
        ];

        return view('landing', [
            "paths" => $data_path,
            "testimonials" => $data_testimonial
        ]);
    }

    public function webinars()
    {
        return view('dashboard.webinars');
    }

    public function webinarFormPage($slug)
    {
        $webinar = Webinar::where('slug', $slug)->firstOrFail();
        return view('webinar_form.webinar', compact('slug'));
    }
}
