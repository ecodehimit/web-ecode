<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webinars', function (Blueprint $table) {
            $table->id();
            $table->string('title', 64);
            $table->text('description');
            $table->string('slug', 64)->unique();
            $table->dateTime('date');
            $table->string('time', 20);
            $table->text('link_meet')->nullable();
            $table->unsignedBigInteger('host_id')->nullable();
            $table->foreign('host_id')->references('id')->on('ecode_members');
            $table->boolean('open_status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webinars');
    }
};
