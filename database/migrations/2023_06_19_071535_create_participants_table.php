<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participants', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('webinar_id');
            $table->foreign('webinar_id')->references('id')->on('webinars');
            $table->text('participant_code');
            $table->string('fullname', 64)->nullable();
            $table->string('from', 64)->nullable();
            $table->boolean('is_it')->default(true);
            $table->boolean('is_pens')->default(true);
            $table->boolean('present_status')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participants');
    }
};
