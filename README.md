# Requirements
- Node.js
- Composer
- PHP >=8.0
- MySQL
- Npm

# Installation
- Clone the repository
- Run `composer install`
- copy `.env.example` to `.env`
- configure your database in `.env`
- Run `php artisan key:generate`
- Run `php artisan migrate`
- Run `npm install && npm run watch`
- Run `php artisan serve`
- enjoy your project
