<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\LandingPageController;
use App\Http\Controllers\LoginController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ActiveMemberController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::controller(LandingPageController::class)->group(function () {
    Route::get("/", "landingPage")->name("landing");
<<<<<<< routes/web.php
    Route::get("/webinars", "webinars")->middleware("login")->name("webinars");
=======
>>>>>>> routes/web.php
    Route::get("/webinar-form/{slug?}", "webinarFormPage")->middleware("login")->name("webinar");
});

Route::prefix("auth")->group(function () {
    Route::prefix("login")->name("login.")->group(function () {
        Route::controller(LoginController::class)->group(function () {
            Route::get("/", "loginPage")->name("page");
            Route::post("/", "loginPost")->name("post");
        });
    });

    Route::prefix("register")->name("register.")->group(function () {
        Route::get('/', function () {
            return view('register.register');
        })->name('page');
    });
});

Route::prefix("/dashboard")
    ->middleware("login")
    ->group(
        function () {
            Route::controller(DashboardController::class)->group(function () {
                Route::get("/", "dashboardPage")->name("dashboard");
                Route::post("/logout", "logout")->name("logout");
            });
<<<<<<< routes/web.php
=======

            Route::prefix("active-members")->name("active-members.")->group(function () {
                Route::controller(ActiveMemberController::class)->group(function () {
                    Route::get("/", "index")->name("index");
                    Route::post("/", "store")->name("store");
                    Route::get('/getData', 'getData')->name('getData');
                    Route::get('/create-or-update', 'createOrUpdate')->name('createOrUpdate');
                    Route::post('/create-or-update', 'createOrUpdatePost')->name('createOrUpdatePost');
                });
            });
>>>>>>> routes/web.php
        }
    );
