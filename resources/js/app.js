require("./bootstrap");
import 'flowbite';

// Variable for navigation bar
const primaryNav = document.querySelector(".primary-navigation");
const navToggle = document.querySelector(".mobile-nav-toggle");
const nav = document.querySelector(".nav");
// Event listener to add an remove attribute from HTML element
navToggle.addEventListener("click", () => {
    const visibility = primaryNav.getAttribute("data-visible");
    // Check whether the toggle has been clicked or not
    if (visibility === "false") {
        primaryNav.setAttribute("data-visible", true);
        navToggle.setAttribute("aria-expanded", true);
        nav.classList.add("nav-open");
    } else if (visibility === "true") {
        primaryNav.setAttribute("data-visible", false);
        navToggle.setAttribute("aria-expanded", false);
        nav.classList.remove("nav-open");
    }
});

// Set tooltip content
const tooltipContent = [
    "Notion",
    "Behance",
    "Figma",
    "Analytic",
    "Github",
    "XAMPP",
    "Postman",
    "Laravel",
    "Tailwind",
    "React",
    "HTML5",
];
// Loop through array
tooltipContent.forEach((x) => tippy(`.${x.toLowerCase()}`, { content: x }));
