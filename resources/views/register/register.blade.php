<!doctype html>
<html lang="id">

<head>
    @include('layouts.app')
    <title>Register</title>
</head>

<body class="h-screen">
    <div class="grid grid-cols-1 md:grid-cols-2 h-full">
        <div class="container p-6 sm:p-8 lg:p-16 xl:px-24 ">
            <form action="" class="xl:w-9/12 lg:w-11/12">
                <a class="flex cursor-pointer" href="{{ route('index') }}">
                    <img src="{{ asset('assets/ecode-logo.svg') }}" alt="Logo Ecode" class="rounded-lg" />
                    <div class="ml-4 align-middle text-2xl font-semibold">E-Code Web</div>
                </a>
                <h1 class="text-4xl font-bold text-black mt-14">Register</h1>
                <p class="text-[#A5A5A5] mt-2 mb-14">Welcome to E-Code Web! 👋</p>
                <div>
                    <label for="email" class="font-bold text-black">Email</label>
                    <input id="email" type="email" placeholder="Email"
                        class="md:rounded-2xl rounded-xl border-[#D2D2D2] border focus:outline-none focus:border focus:border-primary appearance-none block mt-3 w-full py-3 px-4">
                </div>
                <div class="mt-6">
                    <label for="username" class="font-bold text-black">Username</label>
                    <input id="username" type="username" placeholder="Username"
                        class="md:rounded-2xl rounded-xl border-[#D2D2D2] border focus:outline-none focus:border focus:border-primary appearance-none block mt-3 w-full py-3 px-4">
                </div>
                <div class="mt-6 relative">
                    <label for="password" class="font-bold text-black">Password</label>
                    <div class="relative w-full">
                        <div class="absolute inset-y-0 right-0 pr-3 flex items-center">
                            <button class="text-gray-400 focus:outline-none focus:text-primary toggle-password"
                                type="button">
                                <img id="eye-auth" src="{{ asset('assets/authentication/eye-slash.svg') }}"
                                    alt="Toggle Password">
                            </button>
                        </div>
                        <input id="password" type="password" placeholder="Password"
                            class="md:rounded-2xl rounded-xl border-[#D2D2D2] border focus:outline-none focus:border focus:border-primary appearance-none block mt-3 w-full py-3 px-4">
                    </div>
                </div>
                <div class="mt-6 relative">
                    <label for="confirm-password" class="font-bold text-black">Konfirmasi Password</label>
                    <div class="relative w-full">
                        <div class="absolute inset-y-0 right-0 pr-3 flex items-center">
                            <button class="text-gray-400 focus:outline-none focus:text-primary toggle-password-confirm"
                                type="button">
                                <img id="eye-auth-confirm" src="{{ asset('assets/authentication/eye-slash.svg') }}"
                                    alt="Toggle Confirm Password">
                            </button>
                        </div>
                        <input id="confirm-password" type="password" placeholder="Konfirmasi Password"
                            class="md:rounded-2xl rounded-xl border-[#D2D2D2] border focus:outline-none focus:border focus:border-primary appearance-none block mt-3 w-full py-3 px-4">
                    </div>
                </div>
                <button
                    class="mt-16 py-3 px-6 border-white bg-primary md:rounded-2xl rounded-xl text-white w-full font-semibold">Register</button>
                <p class="text-center my-2 text-sm text-[#A5A5A5]">atau</p>
                <button
                    class="py-3 px-6 border-[#D2D2D2] border bg-white md:rounded-2xl rounded-xl text-dark w-full font-semibold">
                    <div class="flex items-center justify-center">
                        <img class="h-5 mr-2 object-contain" src="{{ asset('assets/authentication/google.png') }}"
                            alt="Google">
                        <div class="h-auto font-bold">Register dengan Google</div>
                    </div>
                </button>
                <div class="text-[#A5A5A5] text-center mt-10">Udah ada akun? <span
                        class="text-primary font-semibold cursor-pointer"><a
                            href="{{ route('login') }}">Login</a></span> aja 🗿</div>
            </form>
        </div>
        <div
            class="hidden md:block bg-primary bg-[length:100%_70%] bg-[url('/assets/authentication/auth-bg.png')] bg-no-repeat bg-right-top">
            <div class="grid place-items-center h-full px-10">
                <img src="{{ asset('assets/authentication/auth-image.png') }}" alt="Authentication Image"
                    class="m-auto">
            </div>
        </div>
    </div>
</body>

<script>
    const togglePassword = document.querySelector('.toggle-password');
    const togglePasswordConfirm = document.querySelector('.toggle-password-confirm');
    const passwordInput = document.querySelector('#password');
    const confirmPasswordInput = document.querySelector('#confirm-password');
    const imgAuth = document.querySelector('#eye-auth');
    const imgAuthConfirm = document.querySelector('#eye-auth-confirm');
    const eyeSlashPath = '/assets/authentication/eye-slash.svg';
    const eyePath = '/assets/authentication/eye.svg';

    togglePassword.addEventListener('click', () => {
        const type = passwordInput.getAttribute('type') === 'password' ? 'text' : 'password';
        imgAuth.setAttribute('src', passwordInput.getAttribute('type') === 'password' ? eyePath : eyeSlashPath);
        passwordInput.setAttribute('type', type);
    });

    togglePasswordConfirm.addEventListener('click', () => {
        const type = confirmPasswordInput.getAttribute('type') === 'password' ? 'text' : 'password';
        imgAuthConfirm.setAttribute('src', confirmPasswordInput.getAttribute('type') === 'password' ? eyePath :
            eyeSlashPath);
        confirmPasswordInput.setAttribute('type', type);
    });
</script>

</html>
