@extends('dashboard.template.master')

@section('Title')
    Overview
@endsection

@section('content')
    <div class="grid gap-4 grid-cols-2 place-content-between w-full border-b-2 py-8 px-6 border-dark10">
        <div>
            <h1 class="font-semibold text-2xl mb-1 text-black">Overview</h1>
            <h2 class="text-dark20">Simply everything at a glance.</h2>
        </div>
        <div class="flex items-center justify-end gap-6 content-center">
            <div>
                <input id="search" name="search" type="text" placeholder="Search something..."
                    class="md:rounded-2xl font-medium text-blue-500 rounded-xl border-dark10 border-2 focus:outline-none focus:border focus:border-primary appearance-none block w-full py-3 px-4">
            </div>
            <img class="h-12 w-12 cursor-pointer rounded-full" src="{{ asset('assets/testimonial/profile-1.png') }}"
                alt="Avatar">
            <div class="relative cursor-pointer rounded-full border-dashed border-2 h-12 w-12 border-[#999999] p-3">
                <svg viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M20.1459 15.0937L19.1042 13.3646C18.8854 12.9791 18.6875 12.25 18.6875 11.8229V9.18748C18.6875 6.73956 17.25 4.62498 15.1771 3.6354C14.6354 2.67706 13.6354 2.08331 12.4896 2.08331C11.3542 2.08331 10.3334 2.6979 9.79169 3.66665C7.76044 4.67706 6.35419 6.77081 6.35419 9.18748V11.8229C6.35419 12.25 6.15627 12.9791 5.93752 13.3541L4.88544 15.0937C4.46877 15.7916 4.37502 16.5625 4.63544 17.2708C4.88544 17.9687 5.47919 18.5104 6.25002 18.7708C8.27086 19.4583 10.3959 19.7916 12.5209 19.7916C14.6459 19.7916 16.7709 19.4583 18.7917 18.7812C19.5209 18.5416 20.0834 17.9896 20.3542 17.2708C20.625 16.5521 20.5521 15.7604 20.1459 15.0937Z"
                        fill="#999999" />
                    <path
                        d="M15.4479 20.8437C15.0104 22.0521 13.8542 22.9166 12.5 22.9166C11.6771 22.9166 10.8646 22.5833 10.2917 21.9896C9.95833 21.6771 9.70833 21.2604 9.5625 20.8333C9.69792 20.8541 9.83333 20.8646 9.97917 20.8854C10.2187 20.9166 10.4687 20.9479 10.7187 20.9687C11.3125 21.0208 11.9167 21.0521 12.5208 21.0521C13.1146 21.0521 13.7083 21.0208 14.2917 20.9687C14.5104 20.9479 14.7292 20.9375 14.9375 20.9062C15.1042 20.8854 15.2708 20.8646 15.4479 20.8437Z"
                        fill="#999999" />
                </svg>
                <div
                    class="absolute inline-flex items-center justify-center w-6 h-6 text-sm font-bold text-white bg-red-500 rounded-full -top-2 -right-2">
                    5</div>

            </div>
        </div>
    </div>
    <div class="grid grid-cols-10 gap-8 py-8 px-6">
        <div class="col-span-3">
            <div
                class="cursor-pointer h-48 bg-[url('/assets/dashboard/card-bg.png')] bg-no-repeat bg-auto bg-center rounded-2xl w-full">
                <div class="py-6 px-8 h-full w-full grid grid-rows-3 grid-flow-row place-content-between">
                    <div class="text-white">Incoming Webinar</div>
                    <div>
                        <div class="text-white font-semibold text-xl">Get to Know UI/UX Design</div>
                        <div class="text-white text-xl">Sergio Marquina</div>
                    </div>
                    <div class="flex gap-6">
                        <div class="flex items-end gap-3">
                            <svg width="20" height="19" viewBox="0 0 14 15" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M12.7777 6.60291H0.89502C0.61902 6.60291 0.39502 6.37891 0.39502 6.10291C0.39502 5.82691 0.61902 5.60291 0.89502 5.60291H12.7777C13.0537 5.60291 13.2777 5.82691 13.2777 6.10291C13.2777 6.37891 13.0537 6.60291 12.7777 6.60291"
                                    fill="white" />
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M9.80076 9.20642C9.52476 9.20642 9.2981 8.98242 9.2981 8.70642C9.2981 8.43042 9.51876 8.20642 9.79476 8.20642H9.80076C10.0768 8.20642 10.3008 8.43042 10.3008 8.70642C10.3008 8.98242 10.0768 9.20642 9.80076 9.20642"
                                    fill="white" />
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M6.84227 9.20642C6.56627 9.20642 6.3396 8.98242 6.3396 8.70642C6.3396 8.43042 6.56027 8.20642 6.83627 8.20642H6.84227C7.11827 8.20642 7.34227 8.43042 7.34227 8.70642C7.34227 8.98242 7.11827 9.20642 6.84227 9.20642"
                                    fill="white" />
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M3.87785 9.20642C3.60185 9.20642 3.37451 8.98242 3.37451 8.70642C3.37451 8.43042 3.59585 8.20642 3.87185 8.20642H3.87785C4.15385 8.20642 4.37785 8.43042 4.37785 8.70642C4.37785 8.98242 4.15385 9.20642 3.87785 9.20642"
                                    fill="white" />
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M9.80076 11.7975C9.52476 11.7975 9.2981 11.5735 9.2981 11.2975C9.2981 11.0215 9.51876 10.7975 9.79476 10.7975H9.80076C10.0768 10.7975 10.3008 11.0215 10.3008 11.2975C10.3008 11.5735 10.0768 11.7975 9.80076 11.7975"
                                    fill="white" />
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M6.84227 11.7975C6.56627 11.7975 6.3396 11.5735 6.3396 11.2975C6.3396 11.0215 6.56027 10.7975 6.83627 10.7975H6.84227C7.11827 10.7975 7.34227 11.0215 7.34227 11.2975C7.34227 11.5735 7.11827 11.7975 6.84227 11.7975"
                                    fill="white" />
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M3.87785 11.7975C3.60185 11.7975 3.37451 11.5735 3.37451 11.2975C3.37451 11.0215 3.59585 10.7975 3.87185 10.7975H3.87785C4.15385 10.7975 4.37785 11.0215 4.37785 11.2975C4.37785 11.5735 4.15385 11.7975 3.87785 11.7975"
                                    fill="white" />
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M9.52881 3.86069C9.25281 3.86069 9.02881 3.63669 9.02881 3.36069V1.16669C9.02881 0.890687 9.25281 0.666687 9.52881 0.666687C9.80481 0.666687 10.0288 0.890687 10.0288 1.16669V3.36069C10.0288 3.63669 9.80481 3.86069 9.52881 3.86069"
                                    fill="white" />
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M4.14355 3.86069C3.86755 3.86069 3.64355 3.63669 3.64355 3.36069V1.16669C3.64355 0.890687 3.86755 0.666687 4.14355 0.666687C4.41955 0.666687 4.64355 0.890687 4.64355 1.16669V3.36069C4.64355 3.63669 4.41955 3.86069 4.14355 3.86069"
                                    fill="white" />
                                <mask id="mask0_357_346" style="mask-type:luminance" maskUnits="userSpaceOnUse"
                                    x="0" y="1" width="14" height="14">
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                        d="M0.333252 1.71942H13.3333V15H0.333252V1.71942Z" fill="white" />
                                </mask>
                                <g mask="url(#mask0_357_346)">
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                        d="M4.01392 2.71942C2.28525 2.71942 1.33325 3.64142 1.33325 5.31542V11.3481C1.33325 13.0588 2.28525 14.0001 4.01392 14.0001H9.65259C11.3813 14.0001 12.3333 13.0761 12.3333 11.3988V5.31542C12.3359 4.49209 12.1146 3.85209 11.6753 3.41209C11.2233 2.95875 10.5266 2.71942 9.65859 2.71942H4.01392ZM9.65259 15.0001H4.01392C1.74392 15.0001 0.333252 13.6008 0.333252 11.3481V5.31542C0.333252 3.09675 1.74392 1.71942 4.01392 1.71942H9.65859C10.7979 1.71942 11.7399 2.06075 12.3833 2.70542C13.0079 3.33275 13.3366 4.23475 13.3333 5.31675V11.3988C13.3333 13.6201 11.9226 15.0001 9.65259 15.0001V15.0001Z"
                                        fill="white" />
                                </g>
                            </svg>
                            <div class="text-white">Apr 21</div>
                        </div>
                        <div class="flex items-end gap-3">
                            <svg width="20" height="20" viewBox="0 0 14 14" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M6.99992 1.33331C3.87525 1.33331 1.33325 3.87531 1.33325 6.99998C1.33325 10.1246 3.87525 12.6666 6.99992 12.6666C10.1246 12.6666 12.6666 10.1246 12.6666 6.99998C12.6666 3.87531 10.1246 1.33331 6.99992 1.33331M6.99992 13.6666C3.32392 13.6666 0.333252 10.676 0.333252 6.99998C0.333252 3.32398 3.32392 0.333313 6.99992 0.333313C10.6759 0.333313 13.6666 3.32398 13.6666 6.99998C13.6666 10.676 10.6759 13.6666 6.99992 13.6666"
                                    fill="white" />
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M9.2875 9.46156C9.20017 9.46156 9.11217 9.43889 9.0315 9.39156L6.51817 7.89222C6.3675 7.80156 6.27417 7.63822 6.27417 7.46222V4.23022C6.27417 3.95422 6.49817 3.73022 6.77417 3.73022C7.05084 3.73022 7.27417 3.95422 7.27417 4.23022V7.17822L9.54417 8.53156C9.78084 8.67356 9.85884 8.98022 9.7175 9.21756C9.6235 9.37422 9.4575 9.46156 9.2875 9.46156"
                                    fill="white" />
                            </svg>
                            <div class="text-white">7PM</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-span-3">
            <div class="h-48 bg-primary rounded-2xl w-full py-6 px-8">
                <div
                    class="w-full h-full grid grid-rows-2 grid-flow-col place-content-between font-semibold text-3xl text-white">
                    <div class="self-start">
                        <div class="flex place-content-between">
                            <a href="{{ route('webinar') }}">
                                <svg class="h-12 w-12 cursor-pointer" width="34" height="34" viewBox="0 0 34 34"
                                fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M9.21707 0.333496H24.7671C30.4337 0.333496 33.6671 3.5335 33.6671 9.21683V24.7835C33.6671 30.4335 30.4504 33.6668 24.7837 33.6668H9.21707C3.53374 33.6668 0.33374 30.4335 0.33374 24.7835V9.21683C0.33374 3.5335 3.53374 0.333496 9.21707 0.333496ZM18.3671 18.3835H23.1004C23.8671 18.3668 24.4837 17.7502 24.4837 16.9835C24.4837 16.2168 23.8671 15.6002 23.1004 15.6002H18.3671V10.9002C18.3671 10.1335 17.7504 9.51683 16.9837 9.51683C16.2171 9.51683 15.6004 10.1335 15.6004 10.9002V15.6002H10.8837C10.5171 15.6002 10.1671 15.7502 9.90041 16.0002C9.65041 16.2668 9.50041 16.6152 9.50041 16.9835C9.50041 17.7502 10.1171 18.3668 10.8837 18.3835H15.6004V23.1002C15.6004 23.8668 16.2171 24.4835 16.9837 24.4835C17.7504 24.4835 18.3671 23.8668 18.3671 23.1002V18.3835Z"
                                    fill="white" />
                            </svg>
                            </a>
                            <div class="flex mb-5 -space-x-4">
                                <img class="w-10 h-10 rounded-full" src="{{ asset('assets/testimonial/profile-1.png') }}"
                                    alt="">
                                <img class="w-10 h-10 rounded-full" src="{{ asset('assets/testimonial/profile-2.png') }}"
                                    alt="">
                                <img class="w-10 h-10 rounded-full" src="{{ asset('assets/testimonial/profile-1.png') }}"
                                    alt="">
                                <img class="w-10 h-10 rounded-full" src="{{ asset('assets/testimonial/profile-2.png') }}"
                                    alt="">
                            </div>
                        </div>
                    </div>
                    <div class="self-end">Create New Webinar</div>
                </div>
            </div>
        </div>
        <div class="col-span-3">
            <div class="h-48 bg-white rounded-2xl w-full py-6 px-8 relative">
                <div class="font-semibold text-xl">Active Members</div>
                <div id="controls-carousel" class="relative w-full h-24 mt-6" data-carousel="static">
                    <div class="relative h-24 overflow-hidden rounded-lg">
                        <div class="px-10 hidden ease-in-out" data-carousel-item="active">
                            <div class="h-24 w-full flex gap-4 justify-center items-center">
                                <img class="h-16 w-16" src="{{ asset('assets/testimonial/profile-1.png') }}" alt="Member">
                                <div class="max-w-[110px] overflow-hidden">
                                    <div class="font-semibold truncate">Azis Zuhri</div>
                                    <div class="text-dark30 font-semibold mt-1 truncate">Team Leader</div>
                                </div>
                            </div>
                        </div>
                        <div class="px-10 hidden ease-in-out" data-carousel-item>
                            <div class="h-24 w-full flex gap-4 justify-center items-center">
                                <img class="h-16 w-16" src="{{ asset('assets/testimonial/profile-2.png') }}" alt="Member">
                                <div class="max-w-[110px] overflow-hidden">
                                    <div class="font-semibold truncate">Lorem Ipsum</div>
                                    <div class="text-dark30 font-semibold mt-1 truncate">Team Member</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="button"
                        class="absolute top-0 left-0 z-30 flex items-center justify-center h-full cursor-pointer group focus:outline-none"
                        data-carousel-prev>
                        <span
                            class="inline-flex items-center justify-center w-5 h-5 rounded-full bg-[#130F26] hover:bg-[#2b2b2b] group-focus:outline-none">
                            <svg aria-hidden="true" class="w-4 h-4 text-white dark:text-gray-800" fill="none"
                                stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                    d="M15 19l-7-7 7-7"></path>
                            </svg>
                            <span class="sr-only">Previous</span>
                        </span>
                    </button>
                    <button type="button"
                        class="absolute top-0 right-0 z-30 flex items-center justify-center h-full cursor-pointer group focus:outline-none"
                        data-carousel-next>
                        <span
                            class="inline-flex items-center justify-center w-5 h-5 rounded-full bg-[#130F26] hover:bg-[#2b2b2b] group-focus:outline-none">
                            <svg aria-hidden="true" class="w-4 h-4 text-white dark:text-gray-800" fill="none"
                                stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7">
                                </path>
                            </svg>
                            <span class="sr-only">Next</span>
                        </span>
                    </button>
                </div>

            </div>
        </div>
        <div class="col-span-1">
            <div class="cursor-pointer h-48 bg-white rounded-2xl w-full py-6 px-8">
                <div class="flex justify-center items-center h-full w-full">
                    <svg class="w-8 h-8" width="12" height="22" viewBox="0 0 12 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M0.626389 21.0404C0.271367 20.6854 0.239093 20.1298 0.529565 19.7384L0.626389 19.6262L9.25216 11L0.626389 2.37375C0.271367 2.01873 0.239093 1.46318 0.529565 1.0717L0.626389 0.959539C0.981411 0.604517 1.53696 0.572242 1.92844 0.862715L2.0406 0.959539L11.3739 10.2929C11.729 10.6479 11.7612 11.2034 11.4708 11.5949L11.3739 11.7071L2.0406 21.0404C1.65008 21.4309 1.01691 21.4309 0.626389 21.0404Z" fill="black"/>
                        </svg>    </div>                
            </div>
        </div>
    </div>
    @dump($webinars)
@endsection

@section('css')
@endsection

@section('js')
@endsection
