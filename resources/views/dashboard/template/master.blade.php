<!DOCTYPE html>
<html lang="en">

<head>
    @include('layouts.app')
    <title>Dashboard | @yield('title')</title>
    @yield('css')
</head>

<body class="h-screen">
    <div>
        {{-- Sidebar --}}
        @include('dashboard.template.sidebar')
        {{-- End Sidebar --}}
    </div>
    <div class="sm:ml-64 h-full">
        {{-- Content --}}
        <div class="col-span-5 sm:col-span-4 bg-[#F6F9FE] h-full">
            @yield('content')
        </div>
        {{-- Content --}}
    </div>
</body>
<script src="{{ asset('js/jquery3.6.js') }}"></script>
<script src="{{ asset('js/swal2.js') }}"></script>

@yield('js')
<script>
    const logout = () => {
        Swal.fire({
            title: 'Apakah kamu ingin logout?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Gk jadi!'
        }).then((result) => {
            if (result.isConfirmed) {
                $('#logout').submit();
            }
        });
    }
</script>

</html>