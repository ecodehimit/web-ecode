@extends('dashboard.template.master')

@section('Title')
    Overview
@endsection

@section('content')
    <div class="container">
        <section class="grid grid-cols-2 place-content-center content-center py-10 pl-6 pr-24 border-b border-[#D2D2D2]">
            <div class="space-y-2">
                <h5 class="text-2xl font-semibold">Overview</h5>
                <p class="text-[#A5A5A5]">Simply everything at a glance,</p>
            </div>
            <div class="flex items-center justify-end space-x-7">
                <input type="text" class="w-[315px] border border-[#D2D2D2] rounded-2xl py-3 px-4 italic"
                    placeholder="Search something...">
                <div class="rounded-full w-[50px] h-[50px] overflow-hidden">
                    <img class="w-full h-full object-cover object-center"
                        src="{{ asset('assets/dashboard/profile/profile-1.png') }}" alt="">
                </div>
                <div class="relative border border-dashed p-3 rounded-full w-[50px] h-[50px] border-[#999]">
                    <span
                        class="absolute bg-[#FF5050FA] rounded-full text-[11px] py-0.5 px-2 -top-1 -right-1 text-white">5</span>
                    <svg width="25" height="25" viewBox="0 0 25 25" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M20.1459 15.0937L19.1042 13.3646C18.8854 12.9791 18.6875 12.25 18.6875 11.8229V9.18748C18.6875 6.73956 17.25 4.62498 15.1771 3.6354C14.6354 2.67706 13.6354 2.08331 12.4896 2.08331C11.3542 2.08331 10.3334 2.6979 9.79169 3.66665C7.76044 4.67706 6.35419 6.77081 6.35419 9.18748V11.8229C6.35419 12.25 6.15627 12.9791 5.93752 13.3541L4.88544 15.0937C4.46877 15.7916 4.37502 16.5625 4.63544 17.2708C4.88544 17.9687 5.47919 18.5104 6.25002 18.7708C8.27086 19.4583 10.3959 19.7916 12.5209 19.7916C14.6459 19.7916 16.7709 19.4583 18.7917 18.7812C19.5209 18.5416 20.0834 17.9896 20.3542 17.2708C20.625 16.5521 20.5521 15.7604 20.1459 15.0937Z"
                            fill="#999999" />
                        <path
                            d="M15.4479 20.8437C15.0104 22.0521 13.8542 22.9166 12.5 22.9166C11.6771 22.9166 10.8646 22.5833 10.2917 21.9896C9.95833 21.6771 9.70833 21.2604 9.5625 20.8333C9.69792 20.8541 9.83333 20.8646 9.97917 20.8854C10.2187 20.9166 10.4687 20.9479 10.7187 20.9687C11.3125 21.0208 11.9167 21.0521 12.5208 21.0521C13.1146 21.0521 13.7083 21.0208 14.2917 20.9687C14.5104 20.9479 14.7292 20.9375 14.9375 20.9062C15.1042 20.8854 15.2708 20.8646 15.4479 20.8437Z"
                            fill="#999999" />
                    </svg>
                </div>
            </div>
        </section>
        <section class="pt-10 pl-6 pr-24">
            <div class="card-overview flex space-x-6">
                <div class="incoming-webinar relative rounded-2xl  bg-[#4A4A4A63] overflow-hidden p-4 text-white">
                    <div class="z-10 relative">
                        <p class="text-xs">Incoming Webinar</p>
                        <div class="space-y-1 my-5">
                            <h4 class="font-semibold">Get to Know UI/UX Design</h4>
                            <p class="m-0">Sergio Marquina</p>
                        </div>
                        <div class="flex space-x-5">
                            <div class="flex text-xs">
                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                        d="M13.7777 6.60291H1.89502C1.61902 6.60291 1.39502 6.37891 1.39502 6.10291C1.39502 5.82691 1.61902 5.60291 1.89502 5.60291H13.7777C14.0537 5.60291 14.2777 5.82691 14.2777 6.10291C14.2777 6.37891 14.0537 6.60291 13.7777 6.60291Z"
                                        fill="white" />
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                        d="M10.8008 9.20642C10.5248 9.20642 10.2981 8.98242 10.2981 8.70642C10.2981 8.43042 10.5188 8.20642 10.7948 8.20642H10.8008C11.0768 8.20642 11.3008 8.43042 11.3008 8.70642C11.3008 8.98242 11.0768 9.20642 10.8008 9.20642Z"
                                        fill="white" />
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                        d="M7.84227 9.20642C7.56627 9.20642 7.3396 8.98242 7.3396 8.70642C7.3396 8.43042 7.56027 8.20642 7.83627 8.20642H7.84227C8.11827 8.20642 8.34227 8.43042 8.34227 8.70642C8.34227 8.98242 8.11827 9.20642 7.84227 9.20642Z"
                                        fill="white" />
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                        d="M4.87785 9.20642C4.60185 9.20642 4.37451 8.98242 4.37451 8.70642C4.37451 8.43042 4.59585 8.20642 4.87185 8.20642H4.87785C5.15385 8.20642 5.37785 8.43042 5.37785 8.70642C5.37785 8.98242 5.15385 9.20642 4.87785 9.20642Z"
                                        fill="white" />
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                        d="M10.8008 11.7975C10.5248 11.7975 10.2981 11.5735 10.2981 11.2975C10.2981 11.0215 10.5188 10.7975 10.7948 10.7975H10.8008C11.0768 10.7975 11.3008 11.0215 11.3008 11.2975C11.3008 11.5735 11.0768 11.7975 10.8008 11.7975Z"
                                        fill="white" />
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                        d="M7.84227 11.7975C7.56627 11.7975 7.3396 11.5735 7.3396 11.2975C7.3396 11.0215 7.56027 10.7975 7.83627 10.7975H7.84227C8.11827 10.7975 8.34227 11.0215 8.34227 11.2975C8.34227 11.5735 8.11827 11.7975 7.84227 11.7975Z"
                                        fill="white" />
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                        d="M4.87785 11.7975C4.60185 11.7975 4.37451 11.5735 4.37451 11.2975C4.37451 11.0215 4.59585 10.7975 4.87185 10.7975H4.87785C5.15385 10.7975 5.37785 11.0215 5.37785 11.2975C5.37785 11.5735 5.15385 11.7975 4.87785 11.7975Z"
                                        fill="white" />
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                        d="M10.5288 3.86069C10.2528 3.86069 10.0288 3.63669 10.0288 3.36069V1.16669C10.0288 0.890687 10.2528 0.666687 10.5288 0.666687C10.8048 0.666687 11.0288 0.890687 11.0288 1.16669V3.36069C11.0288 3.63669 10.8048 3.86069 10.5288 3.86069Z"
                                        fill="white" />
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                        d="M5.14355 3.86069C4.86755 3.86069 4.64355 3.63669 4.64355 3.36069V1.16669C4.64355 0.890687 4.86755 0.666687 5.14355 0.666687C5.41955 0.666687 5.64355 0.890687 5.64355 1.16669V3.36069C5.64355 3.63669 5.41955 3.86069 5.14355 3.86069Z"
                                        fill="white" />
                                    <mask id="mask0_262_430" style="mask-type:luminance" maskUnits="userSpaceOnUse"
                                        x="1" y="1" width="14" height="14">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M1.33325 1.71942H14.3333V15H1.33325V1.71942Z" fill="white" />
                                    </mask>
                                    <g mask="url(#mask0_262_430)">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M5.01392 2.71942C3.28525 2.71942 2.33325 3.64142 2.33325 5.31542V11.3481C2.33325 13.0588 3.28525 14.0001 5.01392 14.0001H10.6526C12.3813 14.0001 13.3333 13.0761 13.3333 11.3988V5.31542C13.3359 4.49209 13.1146 3.85209 12.6753 3.41209C12.2233 2.95875 11.5266 2.71942 10.6586 2.71942H5.01392ZM10.6526 15.0001H5.01392C2.74392 15.0001 1.33325 13.6008 1.33325 11.3481V5.31542C1.33325 3.09675 2.74392 1.71942 5.01392 1.71942H10.6586C11.7979 1.71942 12.7399 2.06075 13.3833 2.70542C14.0079 3.33275 14.3366 4.23475 14.3333 5.31675V11.3988C14.3333 13.6201 12.9226 15.0001 10.6526 15.0001Z"
                                            fill="white" />
                                    </g>
                                </svg>
                                <p class="ml-2">Apr 21</p>
                            </div>
                            <div class="flex text-xs">
                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                        d="M7.99992 2.33331C4.87525 2.33331 2.33325 4.87531 2.33325 7.99998C2.33325 11.1246 4.87525 13.6666 7.99992 13.6666C11.1246 13.6666 13.6666 11.1246 13.6666 7.99998C13.6666 4.87531 11.1246 2.33331 7.99992 2.33331ZM7.99992 14.6666C4.32392 14.6666 1.33325 11.676 1.33325 7.99998C1.33325 4.32398 4.32392 1.33331 7.99992 1.33331C11.6759 1.33331 14.6666 4.32398 14.6666 7.99998C14.6666 11.676 11.6759 14.6666 7.99992 14.6666Z"
                                        fill="white" />
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                        d="M10.2875 10.4616C10.2002 10.4616 10.1122 10.4389 10.0315 10.3916L7.51817 8.89222C7.3675 8.80156 7.27417 8.63822 7.27417 8.46222V5.23022C7.27417 4.95422 7.49817 4.73022 7.77417 4.73022C8.05084 4.73022 8.27417 4.95422 8.27417 5.23022V8.17822L10.5442 9.53156C10.7808 9.67356 10.8588 9.98022 10.7175 10.2176C10.6235 10.3742 10.4575 10.4616 10.2875 10.4616Z"
                                        fill="white" />
                                </svg>
                                <p class="ml-2">7pm</p>
                            </div>
                        </div>
                    </div>
                    <img class="w-full h-full top-0 right-0 absolute" src="{{ asset('assets/dashboard/card-bg.jpg') }}"
                        alt="">
                </div>
                <a href="#"
                    class="create-webinar  cursor-pointer relative rounded-2xl  bg-[#2363DE] overflow-hidden p-4 text-white">
                    <div class="flex justify-between">
                        <svg width="40" height="40" viewBox="0 0 40 40" fill="none"
                            xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M12.2171 3.3335H27.7671C33.4337 3.3335 36.6671 6.5335 36.6671 12.2168V27.7835C36.6671 33.4335 33.4504 36.6668 27.7837 36.6668H12.2171C6.53374 36.6668 3.33374 33.4335 3.33374 27.7835V12.2168C3.33374 6.5335 6.53374 3.3335 12.2171 3.3335ZM21.3671 21.3835H26.1004C26.8671 21.3668 27.4837 20.7502 27.4837 19.9835C27.4837 19.2168 26.8671 18.6002 26.1004 18.6002H21.3671V13.9002C21.3671 13.1335 20.7504 12.5168 19.9837 12.5168C19.2171 12.5168 18.6004 13.1335 18.6004 13.9002V18.6002H13.8837C13.5171 18.6002 13.1671 18.7502 12.9004 19.0002C12.6504 19.2668 12.5004 19.6152 12.5004 19.9835C12.5004 20.7502 13.1171 21.3668 13.8837 21.3835H18.6004V26.1002C18.6004 26.8668 19.2171 27.4835 19.9837 27.4835C20.7504 27.4835 21.3671 26.8668 21.3671 26.1002V21.3835Z"
                                fill="white" />
                        </svg>
                        <div class="flex relative w-full">
                            <img class="w-8 h-8 rounded-full absolute top-0 right-16 z-30"
                                src="{{ asset('assets/dashboard/profile/profile-2.png') }}" alt="">
                            <img class="w-8 h-8 rounded-full absolute top-0 right-10 z-20"
                                src="{{ asset('assets/dashboard/profile/profile-3.png') }}" alt="">
                            <img class="w-8 h-8 rounded-full absolute top-0 right-5 z-10"
                                src="{{ asset('assets/dashboard/profile/profile-4.png') }}" alt="">
                            <img class="w-8 h-8 rounded-full absolute top-0 right-0"
                                src="{{ asset('assets/dashboard/profile/profile-1.png') }}" alt="">
                        </div>
                    </div>
                    <h1 class="text-2xl font-semibold w-3/4 mt-[19px]">Create New
                        Webinar</h1>

                </a>
                <div class="active-member rounded-2xl bg-[#fff] overflow-hidden p-4">
                    <h1 class="font-semibold">Active Members</h1>
                    <div class="swiper mySwiper">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="flex space-x-4 items-center">
                                    <div class="w-[58px] h-[58px] rounded-full overflow-hidden bg-[#A7C1F2]">
                                        <img class="object-center object-cover"
                                            src="{{ asset('assets/dashboard/profile/member-aziz.png') }}" alt="">
                                    </div>
                                    <div class="flex flex-col space-y-2">
                                        <h1 class="text-xs font-semibold">Aziz Zuhri</h1>
                                        <p class="text-[#777] text-xs">Team Lead</p>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="flex space-x-4 items-center">
                                    <div class="w-[58px] h-[58px] rounded-full overflow-hidden bg-[#A7C1F2]">
                                        <img class="object-center object-cover"
                                            src="{{ asset('assets/dashboard/profile/member-aziz.png') }}" alt="">
                                    </div>
                                    <div class="flex flex-col space-y-2">
                                        <h1 class="text-xs font-semibold">Aziz Zuhri</h1>
                                        <p class="text-[#777] text-xs">Team Lead</p>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="flex space-x-4 items-center">
                                    <div class="w-[58px] h-[58px] rounded-full overflow-hidden bg-[#A7C1F2]">
                                        <img class="object-center object-cover"
                                            src="{{ asset('assets/dashboard/profile/member-aziz.png') }}" alt="">
                                    </div>
                                    <div class="flex flex-col space-y-2">
                                        <h1 class="text-xs font-semibold">Aziz Zuhri</h1>
                                        <p class="text-[#777] text-xs">Team Lead</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>
                </div>
                <a href="#" class="min-w-max rounded-2xl bg-[#fff] overflow-hidden p-4 flex items-center">
                    <svg width="32" height="32" viewBox="0 0 32 32" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M10.6264 26.0404C10.2714 25.6854 10.2391 25.1298 10.5296 24.7384L10.6264 24.6262L19.2522 16L10.6264 7.37375C10.2714 7.01873 10.2391 6.46318 10.5296 6.0717L10.6264 5.95954C10.9814 5.60452 11.537 5.57224 11.9284 5.86271L12.0406 5.95954L21.3739 15.2929C21.729 15.6479 21.7612 16.2034 21.4708 16.5949L21.3739 16.7071L12.0406 26.0404C11.6501 26.4309 11.0169 26.4309 10.6264 26.0404Z"
                            fill="black" />
                    </svg>
                </a>
            </div>
            <div class="py-[29px] px-5 bg-white mt-6 rounded-2xl">
                <div class="flex justify-between items-center">
                    <h1 class="font-semibold">List of Webinars</h1>
                    <div class="flex space-x-5 items-center text-xs font-semibold ">
                        <h1 class="text-[#4A4A4A] ">Status</h1>
                        <div class="flex space-x-4 status text-[#777]">
                            <a href="#" class="px-7 py-4 border border-[#A5A5A5] rounded-2xl active">
                                All
                            </a>
                            <a href="#" class="px-7 py-4 border border-[#A5A5A5] rounded-2xl ">
                                Incoming
                            </a>
                            <a href="#" class="px-7 py-4 border border-[#A5A5A5] rounded-2xl ">
                                Done
                            </a>
                        </div>
                    </div>
                    <div class="flex space-x-5 items-center text-xs font-semibold ">
                        <h1 class="text-[#4A4A4A] ">Sort by Date</h1>
                        <form action="" method="" class="status text-[#777]">
                            <div class="relative max-w-sm">
                                <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <svg aria-hidden="true" class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd"
                                            d="M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z"
                                            clip-rule="evenodd"></path>
                                    </svg>
                                </div>
                                <input datepicker type="text"
                                    class="bg-gray-50 px-7 py-4 rounded-2xl border border-[#A5A5A5] text-xs focus:ring-[#2363DE] focus:border-[#2363DE] block w-full pl-10 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Choose Webinar Date">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="mt-5">
                    <div class="grid grid-cols-2 gap-5">
                        @php
                            $status = 1;
                        @endphp
                        @for ($i = 1; $i <= 6; $i++)
                            <div class="webinar-card p-4 border border-[#D2D2D2] rounded-2xl">
                                <div class="flex justify-between items-center">
                                    <div class="flex space-x-4 text-xs font-normal">
                                        <p>17/04/2023</p>
                                        <p>19.00</p>
                                    </div>
                                    @if ($status == 1)
                                        <span class="text-[#2363DE] font-semibold text-xs">Incoming</span>
                                        @php
                                            $status = 0;
                                        @endphp
                                    @else
                                        <span class="text-[#22C55E] font-semibold text-xs">Done</span>
                                        @php
                                            $status = 1;
                                        @endphp
                                    @endif
                                </div>
                                <div class="flex space-x-3 mt-2">
                                    <div class="w-14 h-14 overflow-hidden rounded-xl">
                                        <img class="w-full h-full object-cover object-center"
                                            src="{{ asset('assets/dashboard/card-bg.jpg') }}" alt="">
                                    </div>
                                    <div>
                                        <h1 class="font-semibold">Get to Know UI/UX Design</h1>
                                        <div class="flex space-x-2">
                                            <div class="w-4 h-4 overflow-hidden rounded-2xl">
                                                <img class="object-cover object-center"
                                                    src="{{ asset('assets/dashboard/profile/profile-1.png') }}"
                                                    alt="">
                                            </div>
                                            <p class="text-xs font-normal">Sergio Marquina</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="flex justify-between items-center">
                                    <p class="text-xs font-normal text-[#777]">
                                        gtk-uiux
                                    </p>
                                    <div class="flex space-x-3">
                                        <div class="border border-[#A5A5A5] rounded-2xl py-3 px-3">
                                            <svg width="24" height="25" viewBox="0 0 24 25" fill="none"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M12 10.5C11.6044 10.5 11.2178 10.6173 10.8889 10.8371C10.56 11.0568 10.3036 11.3692 10.1522 11.7346C10.0009 12.1001 9.96126 12.5022 10.0384 12.8902C10.1156 13.2781 10.3061 13.6345 10.5858 13.9142C10.8655 14.1939 11.2219 14.3844 11.6098 14.4616C11.9978 14.5387 12.3999 14.4991 12.7654 14.3478C13.1308 14.1964 13.4432 13.94 13.6629 13.6111C13.8827 13.2822 14 12.8956 14 12.5C14 11.9696 13.7893 11.4609 13.4142 11.0858C13.0391 10.7107 12.5304 10.5 12 10.5ZM5 10.5C4.60444 10.5 4.21776 10.6173 3.88886 10.8371C3.55996 11.0568 3.30362 11.3692 3.15224 11.7346C3.00087 12.1001 2.96126 12.5022 3.03843 12.8902C3.1156 13.2781 3.30608 13.6345 3.58579 13.9142C3.86549 14.1939 4.22186 14.3844 4.60982 14.4616C4.99778 14.5387 5.39992 14.4991 5.76537 14.3478C6.13082 14.1964 6.44318 13.94 6.66294 13.6111C6.8827 13.2822 7 12.8956 7 12.5C7 11.9696 6.78929 11.4609 6.41421 11.0858C6.03914 10.7107 5.53043 10.5 5 10.5ZM19 10.5C18.6044 10.5 18.2178 10.6173 17.8889 10.8371C17.56 11.0568 17.3036 11.3692 17.1522 11.7346C17.0009 12.1001 16.9613 12.5022 17.0384 12.8902C17.1156 13.2781 17.3061 13.6345 17.5858 13.9142C17.8655 14.1939 18.2219 14.3844 18.6098 14.4616C18.9978 14.5387 19.3999 14.4991 19.7654 14.3478C20.1308 14.1964 20.4432 13.94 20.6629 13.6111C20.8827 13.2822 21 12.8956 21 12.5C21 11.9696 20.7893 11.4609 20.4142 11.0858C20.0391 10.7107 19.5304 10.5 19 10.5Z"
                                                    fill="#777777" />
                                            </svg>
                                        </div>
                                        <a href="#"
                                            class="py-3 px-7 bg-[#F1582F] rounded-2xl flex justify-center items-center text-white">
                                            <svg width="15" height="15" viewBox="0 0 15 15" fill="none"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                    d="M12.6794 3.27685C12.9226 3.27685 13.125 3.47873 13.125 3.7356V3.9731C13.125 4.22372 12.9226 4.43184 12.6794 4.43184H2.32116C2.07741 4.43184 1.875 4.22372 1.875 3.9731V3.7356C1.875 3.47873 2.07741 3.27685 2.32116 3.27685H4.14348C4.51366 3.27685 4.83581 3.01373 4.91909 2.64249L5.01452 2.21624C5.16284 1.63562 5.65093 1.25 6.20954 1.25H8.79046C9.34299 1.25 9.83656 1.63562 9.9794 2.18562L10.0815 2.64186C10.1642 3.01373 10.4863 3.27685 10.8571 3.27685H12.6794ZM11.7536 11.9588C11.9439 10.1857 12.277 5.9732 12.277 5.9307C12.2892 5.80195 12.2472 5.68008 12.1639 5.58196C12.0746 5.49008 11.9615 5.43571 11.8369 5.43571H3.16783C3.04261 5.43571 2.92347 5.49008 2.84081 5.58196C2.75692 5.68008 2.71559 5.80195 2.72167 5.9307C2.72278 5.93851 2.73474 6.0869 2.75472 6.33497C2.84349 7.437 3.09073 10.5064 3.25049 11.9588C3.36355 13.0288 4.06561 13.7013 5.08254 13.7256C5.86727 13.7437 6.6757 13.75 7.50237 13.75C8.28102 13.75 9.07183 13.7437 9.88087 13.7256C10.933 13.7075 11.6345 13.0469 11.7536 11.9588Z"
                                                    fill="white" />
                                            </svg>
                                            <p class="text-xs font-semibold ml-3">Delete</p>
                                        </a>
                                        <a href="#"
                                            class="py-3 px-7 bg-[#FBBF24] rounded-2xl flex justify-center items-center text-white">
                                            <svg width="15" height="15" viewBox="0 0 15 15" fill="none"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                    d="M5.86025 12.5174L11.3517 5.4159C11.6502 5.03294 11.7563 4.59019 11.6568 4.13937C11.5706 3.72954 11.3186 3.33986 10.9405 3.04424L10.0187 2.31191C9.21616 1.67365 8.22132 1.74084 7.65095 2.47316L7.03415 3.27334C6.95457 3.37345 6.97446 3.52126 7.07395 3.60188C7.07395 3.60188 8.63252 4.85154 8.66568 4.87841C8.7718 4.97919 8.85139 5.11356 8.87128 5.27481C8.90444 5.59058 8.68558 5.8862 8.3606 5.92651C8.20806 5.94667 8.06215 5.89964 7.95604 5.8123L6.31787 4.50889C6.23829 4.4491 6.11891 4.46186 6.05258 4.54248L2.15946 9.58142C1.90744 9.89719 1.82122 10.307 1.90744 10.7034L2.40485 12.8601C2.43138 12.9743 2.53087 13.0549 2.65025 13.0549L4.83888 13.0281C5.23682 13.0213 5.60822 12.8399 5.86025 12.5174ZM8.92481 11.8458H12.4936C12.8418 11.8458 13.125 12.1327 13.125 12.4854C13.125 12.8388 12.8418 13.125 12.4936 13.125H8.92481C8.57661 13.125 8.29342 12.8388 8.29342 12.4854C8.29342 12.1327 8.57661 11.8458 8.92481 11.8458Z"
                                                    fill="white" />
                                            </svg>

                                            <p class="text-xs font-semibold ml-3">Edit</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endfor
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('swiper/swiper-bundle.min.css') }}">
    <style>
        .card-overview .incoming-webinar,
        .card-overview .active-member,
        .card-overview .create-webinar {
            height: 150px;
            width: 100%;
        }

        .swiper {
            width: 100%;
            height: 100%;
        }

        .swiper-slide {
            text-align: center;
            font-size: 18px;
            background: #fff;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .swiper-slide img {
            display: block;
            width: 100%;
            height: 100%;
            object-fit: cover;
        }

        .swiper-button-next,
        .swiper-button-prev {
            color: white !important;
            background-color: black;
            border-radius: 100%;
            width: auto;
            height: auto;
            padding: 8px 10px
        }

        .swiper-button-next:after,
        .swiper-button-prev:after {
            font-size: 10px;
        }

        .status a {
            transition: 0.2s all ease-in-out
        }

        .status a.active {
            border: 1px solid #2363DE;
            color: #2363DE;
            background-color: #D3E0F8;
        }

        .status a:hover {
            border: 1px solid #2363DE;
            color: #2363DE;
            background-color: #D3E0F8;
        }
    </style>
@endsection

@section('js')
    <script src="{{ asset('swiper/swiper-bundle.min.js') }}"></script>
    <script src="{{ asset('vendor/flowbite/datepicker.min.js') }}"></script>
    <script>
        var swiper = new Swiper(".mySwiper", {
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
        });
    </script>
@endsection
