<!doctype html>
<html lang="id">

<head>
    @include('layouts.app')
    <title>Webinar Form</title>
    <style>
        .custom-width {
            width: 40% !important;
        }

        @media (max-width: 1200px) {
            .custom-width {
                width: 50%!important;
            }
        }


        @media (max-width: 900px) {
            .custom-width {
                width: 60%!important;
            }
        }

        @media (max-width: 767px) {
            .custom-width {
                width: 100%!important;
            }
        }
    </style>
</head>

<body class="h-full bg-[#F6F9FE]">
    <div class="h-full custom-width bg-white mx-auto py-12 px-8">
        <a class="flex cursor-pointer" href="{{ route('landing') }}">
            <img src="{{ asset('assets/ecode-logo.svg') }}" alt="Logo Ecode" class="rounded-lg" />
            <div class="ml-4 align-middle text-2xl font-semibold">E-Code Webinar</div>
        </a>
        <h1 class="text-4xl mt-14 font-semibold">Get to Know UI/UX Design</h1>
        <div class="h-24 w-full flex gap-4 items-center">
            <img class="h-16 w-16" src="{{ asset('assets/testimonial/profile-1.png') }}" alt="Member">
            <div>
                <div class="font-semibold text-lg">Sergio Marquina</div>
                <div class="text-dark30">UI Designer at Atlassian</div>
            </div>
        </div>
        <div class="w-full my-6">
            <ol class="items-center w-64 grid grid-cols-2 place-content-center mx-auto">
                <li
                    class="grid grid-rows-2 grid-flow-col place-content-center justify-center text-blue-600 dark:text-blue-500">
                    <span
                        class="self-center text-white place-self-center flex items-center justify-center w-8 h-8 rounded-full shrink-0 bg-primary"
                        id="biodata-text">
                        1
                    </span>
                    <span class="self-center place-self-center mt-1.5">
                        <h3 class="font-semibold text-primary text-center text-sm">Biodata</h3>
                    </span>
                </li>
                <li class="grid grid-rows-2 grid-flow-col place-content-center justify-center">
                    <span
                        class="self-center text-white place-self-center flex items-center justify-center w-8 h-8 rounded-full shrink-0 bg-gray-500"
                        id="detail-1">
                        2
                    </span>
                    <span class="self-center place-self-center mt-1.5">
                        <h3 class="font-semibold text-dark30 text-center text-sm" id="detail-2">Detail Institusi</h3>
                    </span>
                </li>
            </ol>
        </div>
        <form action="#" id="form">
            <div id="first-step">
                <div>
                    <label for="name" class="font-bold text-dark50">Nama Lengkap</label>
                    <input id="name" name="name" type="text" placeholder="Masukkan nama lengkapmu"
                        class="md:rounded-2xl rounded-xl border-[#D2D2D2] border focus:outline-none focus:border focus:border-primary appearance-none block mt-3 w-full py-3 px-4"
                        required>
                    <div class="mt-1 text-red-500 text-sm" id="name-error"></div>
                </div>
                <div class="my-6">
                    <label for="email" class="font-bold text-dark50">Email</label>
                    <input id="email" name="email" type="email" placeholder="e.g johndoe@gmail.com"
                        class="md:rounded-2xl rounded-xl border-[#D2D2D2] border focus:outline-none focus:border focus:border-primary appearance-none block mt-3 w-full py-3 px-4"
                        required>
                    <div class="mt-1 text-red-500 text-sm" id="email-error"></div>
                </div>
                <div>
                    <label for="institusi" class="font-bold text-dark50">Asal Institusi</label>
                    <select id="institusi" name="institusi" type="text" placeholder="Asal Institusi"
                        class="md:rounded-2xl rounded-xl border-[#D2D2D2] border focus:outline-none focus:border focus:border-primary appearance-none block mt-3 w-full py-3 px-4">
                        <option selected value="IT PENS">IT PENS
                        </option>
                        <option value="Politeknik Elektronika Negeri Surabaya">Politeknik Elektronika Negeri Surabaya
                        </option>
                        <option value="Lainnya">Lainnya</option>
                    </select>
                </div>
            </div>
            <div id="second-step">
                <div class="fields">
                    <label for="kelas" class="font-bold text-dark50">Kelas</label>
                    <select id="kelas" name="kelas" type="text"
                        class="md:rounded-2xl rounded-xl border-[#D2D2D2] border focus:outline-none focus:border focus:border-primary appearance-none block mt-3 w-full py-3 px-4">
                        <option selected value="D4 A">D4 A
                        </option>
                        <option value="D4 B">D4 A
                        </option>
                        <option value="D3 A">D3 A</option>
                        </option>
                        <option value="D3 B">D3 B</option>
                    </select>
                    <div class="mt-1 text-red-500 text-sm" id="kelas-error"></div>
                </div>
                <div class="fields">
                    <label for="kelas" class="font-bold text-dark50">Kelas</label>
                    <input id="kelas" name="kelas" type="text" placeholder="e.g D4 CE B"
                        class="md:rounded-2xl rounded-xl border-[#D2D2D2] border focus:outline-none focus:border focus:border-primary appearance-none block mt-3 w-full py-3 px-4">
                    <div class="mt-1 text-red-500 text-sm" id="kelas-error"></div>
                </div>
                <div class="my-6 fields">
                    <label for="angkatan" class="font-bold text-dark50">Angkatan</label>
                    <input id="angkatan" name="angkatan" type="text" placeholder="e.g 2022"
                        class="md:rounded-2xl rounded-xl border-[#D2D2D2] border focus:outline-none focus:border focus:border-primary appearance-none block mt-3 w-full py-3 px-4">
                    <div class="mt-1 text-red-500 text-sm" id="angkatan-error"></div>
                </div>
                <div class="fields">
                    <label for="instansi" class="font-bold text-dark50">Asal Instansi</label>
                    <input id="instansi" name="instansi" type="text"
                        placeholder="e.g University of California Berkeley"
                        class="md:rounded-2xl rounded-xl border-[#D2D2D2] border focus:outline-none focus:border focus:border-primary appearance-none block mt-3 w-full py-3 px-4">
                    <div class="mt-1 text-red-500 text-sm" id="instansi-error"></div>
                </div>
            </div>
            <input name="slug" type="hidden" value="{{ $slug }}">
            <button id="btn-next"
                class="my-10 py-3.5 px-6 border-white bg-primary md:rounded-2xl rounded-xl text-white w-full font-semibold">Selanjutnya</button>
        </form>
    </div>

    <script>
        var firstTab = true;
        const firstStep = document.getElementById("first-step");
        const secondStep = document.getElementById("second-step");
        const btnNext = document.getElementById("btn-next");

        const form = document.getElementById("form");
        const nameInput = document.getElementById("name");
        const nameError = document.getElementById("name-error");
        const emailInput = document.getElementById("email");
        const emailError = document.getElementById("email-error");

        const biodataText = document.getElementById("biodata-text");
        const detail1 = document.getElementById("detail-1");
        const detail2 = document.getElementById("detail-2");
        const institusi = document.getElementById("institusi");
        const fields = document.getElementsByClassName("fields");

        const field1 = fields[0];
        const field2 = fields[1];
        const field3 = fields[2];
        const field4 = fields[3];

        updateTabView();

        btnNext.addEventListener('click', function(event) {
            event.preventDefault();
            var isValid = true;
            if (!emailInput.checkValidity()) {
                isValid = false;
                emailError.innerHTML = 'Invalid email';
            } else {
                emailError.innerHTML = '';
            }

            if (!nameInput.checkValidity()) {
                isValid = false;
                nameError.innerHTML = 'Invalid name';
            } else {
                nameError.innerHTML = '';
            }

            if (firstTab && isValid) {
                firstTab = false;
                btnNext.innerHTML = 'Daftar';

                if (institusi.value == 'IT PENS') {
                    field2.remove();
                    field4.remove();
                } else if (institusi.value == 'Politeknik Elektronika Negeri Surabaya') {
                    field1.remove();
                    field4.remove();
                } else {
                    field1.remove();
                    field2.remove();
                    field3.remove();
                }
                detail1.classList.remove('bg-gray-500');
                detail1.classList.add('bg-primary');

                detail2.classList.remove('text-dark30');
                detail2.classList.add('text-primary');

                biodataText.innerHTML =
                    `<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M5.1135 1.33325H10.8935C13.1535 1.33325 14.6668 2.91992 14.6668 5.27992V10.7273C14.6668 13.0799 13.1535 14.6666 10.8935 14.6666H5.1135C2.8535 14.6666 1.3335 13.0799 1.3335 10.7273V5.27992C1.3335 2.91992 2.8535 1.33325 5.1135 1.33325ZM7.62016 9.99325L10.7868 6.82659C11.0135 6.59992 11.0135 6.23325 10.7868 5.99992C10.5602 5.77325 10.1868 5.77325 9.96016 5.99992L7.20683 8.75325L6.04016 7.58659C5.8135 7.35992 5.44016 7.35992 5.2135 7.58659C4.98683 7.81325 4.98683 8.17992 5.2135 8.41325L6.80016 9.99325C6.9135 10.1066 7.06016 10.1599 7.20683 10.1599C7.36016 10.1599 7.50683 10.1066 7.62016 9.99325Z" fill="white"/>
</svg>
`;
                updateTabView();
            }
        });

        function changeTab(value) {
            firstTab = value;
            updateTabView();
        }

        function updateTabView() {
            if (firstTab) {
                firstStep.classList.remove('hidden');
                secondStep.classList.add('hidden');
            } else {
                secondStep.classList.remove('hidden');
                firstStep.classList.add('hidden');
            }
        }
    </script>
</body>

</html>
