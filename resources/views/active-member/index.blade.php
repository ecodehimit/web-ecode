@extends('dashboard.template.master')

@section('Title')
Overview
@endsection

@section('content')
<div class="grid gap-4 grid-cols-2 place-content-between w-full border-b-2 py-8 px-6 border-dark10">
    <div>
        <h1 class="font-semibold text-2xl mb-1 text-black">Active Members</h1>
        <h2 class="text-dark20">Our diserve team of problem solver</h2>
    </div>
    <div class="flex items-center justify-end gap-6 content-center">
        <div>
            <input id="search" name="search" type="text" placeholder="Search something..." class="md:rounded-2xl font-medium text-blue-500 rounded-xl border-dark10 border-2 focus:outline-none focus:border focus:border-primary appearance-none block w-full py-3 px-4">
        </div>
        <img class="h-12 w-12 cursor-pointer rounded-full" src="{{ asset('assets/testimonial/profile-1.png') }}" alt="Avatar">
        <div class="relative cursor-pointer rounded-full border-dashed border-2 h-12 w-12 border-[#999999] p-3">
            <svg viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M20.1459 15.0937L19.1042 13.3646C18.8854 12.9791 18.6875 12.25 18.6875 11.8229V9.18748C18.6875 6.73956 17.25 4.62498 15.1771 3.6354C14.6354 2.67706 13.6354 2.08331 12.4896 2.08331C11.3542 2.08331 10.3334 2.6979 9.79169 3.66665C7.76044 4.67706 6.35419 6.77081 6.35419 9.18748V11.8229C6.35419 12.25 6.15627 12.9791 5.93752 13.3541L4.88544 15.0937C4.46877 15.7916 4.37502 16.5625 4.63544 17.2708C4.88544 17.9687 5.47919 18.5104 6.25002 18.7708C8.27086 19.4583 10.3959 19.7916 12.5209 19.7916C14.6459 19.7916 16.7709 19.4583 18.7917 18.7812C19.5209 18.5416 20.0834 17.9896 20.3542 17.2708C20.625 16.5521 20.5521 15.7604 20.1459 15.0937Z" fill="#999999" />
                <path d="M15.4479 20.8437C15.0104 22.0521 13.8542 22.9166 12.5 22.9166C11.6771 22.9166 10.8646 22.5833 10.2917 21.9896C9.95833 21.6771 9.70833 21.2604 9.5625 20.8333C9.69792 20.8541 9.83333 20.8646 9.97917 20.8854C10.2187 20.9166 10.4687 20.9479 10.7187 20.9687C11.3125 21.0208 11.9167 21.0521 12.5208 21.0521C13.1146 21.0521 13.7083 21.0208 14.2917 20.9687C14.5104 20.9479 14.7292 20.9375 14.9375 20.9062C15.1042 20.8854 15.2708 20.8646 15.4479 20.8437Z" fill="#999999" />
            </svg>
            <div class="absolute inline-flex items-center justify-center w-6 h-6 text-sm font-bold text-white bg-red-500 rounded-full -top-2 -right-2">
                5</div>

        </div>
    </div>
</div>

<a href="{{ route('active-members.createOrUpdate') }}">
    <button class="mt-3 ml-3 bg-primary text-white rounded-xl py-3 px-6 font-semibold">Add New Member</button>
</a>
<div class="container mx-auto px-6 py-8">
    <div class="grid grid-cols-5 gap-4">
        @foreach ($activeMembers as $members)
        <div class="col">
            <div class="bg-white shadow-lg rounded-lg overflow-hidden">
                <img src="{{ asset('images/'.$members->image) }}" class="w-full h-40 object-cover" alt="Foto Anggota">
                <div class="p-4">
                    <div class="flex items-center justify-between mb-2">
                        <div class="flex items-center">
                            <div class="ml-2">
                                <p class="text-sm font-medium text-gray-900">{{ $members->name }}</p>
                                <small class="text-sm font-medium text-gray-500">{{ $members->position }}</small>
                                <div class="flex gap-2 mt-2">
                                    <a href="https://www.instagram.com/{{ $members->instagram }}" target="_blank">
                                        <small class="text-sm font-medium text-gray-500">
                                            <img src="https://img.icons8.com/fluent/48/000000/instagram-new.png" class="w-5 h-5" alt="Instagram">
                                        </small>
                                    </a>
                                    <a href="{{ $members->linkedin_url }}" target="_blank">
                                        <small class="text-sm font-medium text-gray-500">
                                            <img src="https://img.icons8.com/color/48/000000/linkedin.png" class="w-5 h-5" alt="Linkedin">
                                        </small>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="p-3 bg-gray-200 rounded-full">
                            <svg class="w-5 h-5 text-gray-600" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" viewBox="0 0 24 24" stroke="currentColor">
                                <path d="M12 6v6m0 0v6m0-6h6m-6 0H6"></path>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>


@endsection

@section('css')

@endsection

@section('js')

@endsection