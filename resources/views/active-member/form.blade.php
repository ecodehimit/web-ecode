@extends('dashboard.template.master')

@section('Title')
Overview
@endsection

@section('content')
<div class="col-span-5 sm:col-span-4 h-full p-8">
    <form id="form-members" method="POST">
        <div class="mb-4">
            <label class="block text-gray-700 text-sm font-bold mb-2" for="name">
                Nama :
            </label>
            <select class="md:rounded-2xl font-medium text-blue-500 rounded-xl border-dark10 border-2 focus:outline-none focus:border focus:border-primary appearance-none block w-full py-3 px-4" id="nama">
                <option value="" disabled selected class="text-gray-400">Pilih Nama</option>
                @foreach ($allMembers as $member)
                <option value="{{ $member->user_id }}">{{ $member->name }}</option>
                @endforeach
            </select>

            <input type="hidden" id="user_id" name="user_id">
        </div>
        <div class="mb-4">
            <label class="block text-gray-700 text-sm font-bold mb-2" for="college_year">
                Angkatan :
            </label>
            <input class="md:rounded-2xl font-medium text-blue-500 rounded-xl border-dark10 border-2 focus:outline-none focus:border focus:border-primary appearance-none block w-full py-3 px-4" id="college_year" type="number" placeholder="Angkatan" name="college_year">
        </div>
        <div class="mb-4">
            <label class="block text-gray-700 text-sm font-bold mb-2" for="posisi">
                Posisi :
            </label>
            <input class="md:rounded-2xl font-medium text-blue-500 rounded-xl border-dark10 border-2 focus:outline-none focus:border focus:border-primary appearance-none block w-full py-3 px-4" id="posisi" type="text" placeholder="Devisi" name="position">
        </div>
        <div class="mb-4">
            <label class="block text-gray-700 text-sm font-bold mb-2" for="github">
                Github :
            </label>
            <input class="md:rounded-2xl font-medium text-blue-500 rounded-xl border-dark10 border-2 focus:outline-none focus:border focus:border-primary appearance-none block w-full py-3 px-4" id="github" type="text" placeholder="Github" name="github">
        </div>
        <div class="mb-4">
            <label class="block text-gray-700 text-sm font-bold mb-2" for="instagram">
                Instagram :
            </label>
            <input class="md:rounded-2xl font-medium text-blue-500 rounded-xl border-dark10 border-2 focus:outline-none focus:border focus:border-primary appearance-none block w-full py-3 px-4" id="instagram" type="text" placeholder="instagram" name="instagram">
        </div>
        <div class="mb-4">
            <label class="block text-gray-700 text-sm font-bold mb-2" for="linkedin_url">
                Linkedin :
            </label>
            <input class="md:rounded-2xl font-medium text-blue-500 rounded-xl border-dark10 border-2 focus:outline-none focus:border focus:border-primary appearance-none block w-full py-3 px-4" id="linkedin_url" type="text" placeholder="Linkedin url" name="linkedin_url">
        </div>
        <div class="mb-4">
            <label class="block text-gray-700 text-sm font-bold mb-2" for="status">
                Status :
            </label>
            <select class="md:rounded-2xl font-medium text-blue-500 rounded-xl border-dark10 border-2 focus:outline-none focus:border focus:border-primary appearance-none block w-full py-3 px-4" id="status">
                <option value="" disabled selected class="text-gray-400">-- Pilih Status --</option>
                <option value="1" class="text-gray-400">Aktif</option>
                <option value="0" class="text-gray-400">Tidak Aktif</option>
            </select>
        </div>
        <div class="mb-4">
            <label class="block text-gray-700 text-sm font-bold mb-2" for="status">
                Foto :
            </label>
            <input type="file" name="photo" id="photo" class="md:rounded-2xl font-medium text-blue-500 rounded-xl border-dark10 border-2 focus:outline-none focus:border focus:border-primary appearance-none block w-full py-3 px-4">
            <small class="text-gray-400">*Kosongkan jika tidak ingin mengubah foto</small>
        </div>
        <div class="flex items-center justify-end">
            <button class="mt-3 ml-3 bg-primary text-white rounded-xl py-3 px-6 font-semibold hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline mb-4" type="submit">
                Submit
            </button>
        </div>
    </form>
</div>
@endsection

@section('css')

@endsection

@section('js')
<script src="https://code.jquery.com/jquery-3.7.0.min.js" integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).ready(function() {
        $('#nama').change(function() {
            user_id = $(this).val();
            $('#user_id').val(user_id);
            $.ajax({
                url: "{{ route('active-members.getData') }}",
                method: "GET",
                data: {
                    user_id: user_id
                },
                success: function(response) {
                    new Promise(function(resolve, reject) {
                        setTimeout(function() {
                            resolve(swal.close());
                        }, 700);
                    }).then(function() {
                        data = response.data;
                        console.log(data);
                        $('#college_year').val(data.college_year);
                        $('#posisi').val(data.position);
                        $('#github').val(data.github);
                        $('#instagram').val(data.instagram);
                        $('#linkedin_url').val(data.linkedin_url);
                        $('#status').val(data.status);
                    });
                },
                beforeSend: function() {
                    $('#college_year').val('');
                    $('#posisi').val('');
                    $('#github').val('');
                    $('#instagram').val('');
                    $('#linkedin_url').val('');
                    $('#status').val('');
                    swal({
                        title: "Loading...",
                        text: "Mohon tunggu sebentar",
                        icon: "info",
                        buttons: false,
                        closeOnClickOutside: false,
                        closeOnEsc: false,
                    });
                },
                error: function(response) {
                    swal({
                        title: "Error!",
                        text: "Terjadi kesalahan",
                        icon: "error",
                        buttons: false,
                        closeOnClickOutside: false,
                        closeOnEsc: false,
                    });
                }
            });
        });
    });

    $('#form-members').on('submit', function(e) {
        e.preventDefault();
        var id_user = $('#user_id').val();

        var formData = new FormData();
        formData.append('user_id', $('#user_id').val());
        formData.append('college_year', $('#college_year').val());
        formData.append('position', $('#posisi').val());
        formData.append('github', $('#github').val());
        formData.append('instagram', $('#instagram').val());
        formData.append('linkedin_url', $('#linkedin_url').val());
        formData.append('status', $('#status').val());
        formData.append('image', $('#photo')[0].files[0]);

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            url: "{{ route('active-members.createOrUpdatePost') }}",
            method: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function(response) {
                swal({
                    title: "Success!",
                    text: response.message,
                    icon: "success",
                    buttons: true,
                    closeOnClickOutside: true,
                    closeOnEsc: false,
                }).then((value) => {
                    window.location.href = "{{ route('active-members.index') }}";
                });
            },
            beforeSend: function() {
                swal({
                    title: "Loading...",
                    text: "Mohon tunggu sebentar",
                    icon: "info",
                    buttons: false,
                    closeOnClickOutside: false,
                    closeOnEsc: false,
                });
            },
            error: function(response) {
                swal({
                    title: "Error!",
                    text: response.responseJSON.message,
                    icon: "error",
                    buttons: true,
                    closeOnClickOutside: true,
                    closeOnEsc: false,
                });
            }
        });
    })
</script>
@endsection