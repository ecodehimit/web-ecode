@extends('layouts.main')

@section('content')
<body class="relative bg-[#F6F9FE] overflow-x-hidden">
    <section class="flex min-h-screen justify-center items-center bg-primary">
        <img src="{{ asset('assets/pattern.svg') }}" class="absolute h-screen top-0 left-0 z-0 opacity-100">
        <div class="w-full z-10 container px-6 sm:px-8 lg:px-16 xl:px-24">
            <div class="flex justify-center lg:justify-between items-center">
                <div class="lg:block hidden lg:w-1/2">
                    <img src="{{ asset('assets/hero-dummy.png') }}" class="w-11/12">
                </div>
                <div class="text-white lg:text-right text-center lg:w-1/2">
                    <h1 class="sm:text-5xl lg:leading-snug text-3xl leading-normal sm:leading-snug">Hi, Selamat Datang di</h1>
                    <h1 class="font-semibold sm:text-5xl lg:leading-snug text-4xl leading-normal sm:leading-snug">Web E-Code</h1>
                    <div class="flex flex-col lg:flex-row gap-3 mt-8 mb-16 lg:justify-end">
                        <button class="border-2 py-3 px-6 border-white md:rounded-2xl rounded-xl w-auto">Lihat Tutorial <span class="ml-3">></span></button>
                        <button class="border-2 py-3 px-6 border-white bg-white md:rounded-2xl rounded-xl text-dark font-semibold w-auto">Upload Isu Code!</button>
                    </div>
                    <div class="flex lg:justify-end justify-center w-full">
                        <div>
                            <h2 class="text-5xl font-semibold mb-3">20+</h2>
                            <h3>Tim E-Code</h3>
                        </div>
                        <div class="text-6xl mx-6 font-extralight">|</div>
                        <div>
                            <h2 class="text-5xl font-semibold mb-3">50+</h2>
                            <h3>Code Solved</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="w-screen flex justify-center max-w-full bg-white md:py-14 py-10">
        <div>
            <h2 class="md:text-3xl text-2xl font-semibold text-center md:mb-10 mb-4">Mereka yang Telah Percaya</h2>
            <div class="wrapper flex items-center flex-wrap md:space-x-16 space-x-6 width-full justify-center max-w-full">
                <img src="{{ asset('assets/mereka-yang-telah-percaya/traveloka.png') }}" alt="Traveloka" class="w-24 md:w-auto my-2">
                <img src="{{ asset('assets/mereka-yang-telah-percaya/gojek.png') }}" alt="Gojek" class="w-24 md:w-auto my-2">
                <img src="{{ asset('assets/mereka-yang-telah-percaya/telkom.png') }}" alt="Telkom" class="w-24 md:w-auto my-2">
                <img src="{{ asset('assets/mereka-yang-telah-percaya/pln.png') }}" alt="PLN" class="w-24 md:w-auto my-2">
            </div>
        </div>
    </section>

    <section class="mx-auto py-16 px-6 sm:px-8 lg:px-16 xl:px-24 container grid-cols-[3fr,_2fr] md:grid md:gap-16 xl:gap-20">
        <div class="caption h-full flex flex-col justify-center md:items-start items-center">
            <h2 class="font-semibold md:text-4xl text-3xl mb-4 md:mb-8">Tentang E-Code</h2>
            <p class="text-dark lg:text-xl text-center md:text-left lg:pr-28">E-Code merupakan ormawa independen dibawah naungan HIMIT PENS. Fungsi utama E-Code adalah memecahkan masalah dengan pendekatan perangkat lunak.</p>
            <div class="flex space-x-12 md:mt-14 mt-8">
                <div class="projek">
                    <h4 class="font-semibold text-4xl">10+</h4>
                    <p>Projek</p>
                </div>
                <div class="team">
                    <h4 class="font-semibold text-4xl">20+</h4>
                    <p>Tim E-Code</p>
                </div>
                <div class="role">
                    <h4 class="font-semibold text-4xl">5+</h4>
                    <p>Role</p>
                </div>
            </div>
        </div>
        <div class="mt-16 md:mt-0 h-full flex gap-x-3">
            <div class="flex items-center w-[44%] md:w-full lg:w-[44%]">
                <img src="{{ asset('assets/tentang-ecode/illustration2.png') }}" alt="illustration 1" class="w-full rounded-2xl hover:scale-[1.02] transition duration-110">
            </div>
            <div class="lg:flex items-center w-[56%] md:w-full lg:w-[56%] md:hidden flex">
                <img src="{{ asset('assets/tentang-ecode/illustration1.png') }}" alt="illustration 2" class="w-full rounded-2xl hover:scale-[1.02] transition duration-110">
            </div>
        </div>
    </section>

    <section class="mx-auto py-16 px-6 sm:px-8 lg:px-16 xl:px-24 container flex flex-col items-center justify-center">
        <div class="w-full mx-5 mb-10">
            <h3 class="text-primary text-xl font-semibold mb-2 text-center md:text-left">FOLLOW THE ROADMAP</h3>
            <h2 class="md:text-4xl text-3xl font-semibold leading-snug text-center md:text-left">Temukan Alur Belajar Impianmu</h2>
        </div>
        <div class="w-full">
            <div class="path-wrapper flex flex-row gap-4 items-center overflow-x-auto pb-16">
                @foreach($paths as $path)
                    <div>
                        <a href="#">
                            <div class="w-[260px] lg:w-auto md:p-6 p-5 pb-6 md:pb-7 lg:pb-6 lg:p-5 rounded-2xl bg-white h-full shadow-primary">
                                <img src="{{ $path["thumbnail"] }}" class="rounded-xl w-full mb-5 md:mb-6">
                                <h4 class="text-xl mb-1 font-semibold">{{ $path["title"] }}</h4>
                                <p class="text-dark text-base">{{ $path["description"] }}</p>
                                <div class="logo-group mt-6 flex gap-x-4">
                                    @for ($i = 0; $i < count($path["tech"]); $i++)
                                        <img class="h-6 w-6 {{ $path["tech"][$i] }}" src="{{ asset('assets/tech-icon/'.$path["tech"][$i].'.svg') }}">
                                    @endfor
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
        <button class="hover:bg-primary hover:text-white transition-all duration-100 md:px-12 px-8 md:py-3 py-2 border-2 border-primary text-primary rounded-xl">Roadmap Lainnya <span class="ml-3">></span></button>
    </section>

    <section class="bg-white shadow-primary">
        <div class="container mx-auto py-20 px-6 sm:px-8 lg:px-16 xl:px-24 flex flex-col justify-center">
            <div class="text-center mx-5 mb-10 md:mb-20">
                <h3 class="text-primary text-xl font-semibold mb-2">DROP UR ISSUE!</h3>
                <h2 class="md:text-4xl text-3xl font-semibold leading-snug">Diskusikan Isu Kodingmu di sini!</h2>
            </div>
            <div class="flex flex-col md:flex-row-reverse justify-center md:gap-x-8 lg:gap-x-0 mb-16">
                <div class="mb-12 md:mb-0 flex items-center md:w-1/2 xl:w-auto">
                    <img class="md:ml-auto md:mr-0 w-full md:w-[70%]" src="{{ asset('assets/isu-koding/image.png') }}" alt="Upload Isu Kodingmu!">
                </div>
                <div class="flex flex-col justify-center md:w-1/2 gap-y-16 lg:w-auto relative border-dashed border-l border-gray ml-5">
                    <div class="flex gap-x-4 md:gap-x-1 lg:gap-x-4 items-center ml-6">
                        <span class="absolute -left-5 px-4 py-2 bg-primary rounded-xl text-white font-semibold text-lg">1</span>
                        <span class="ml-4 font-bold text-2xl">Upload Isu Code-mu</span>
                    </div>
                    <div class="flex gap-x-4 md:gap-x-1 lg:gap-x-4 items-center ml-6">
                        <span class="absolute -left-5 px-4 py-2 bg-white rounded-xl font-semibold text-lg">2</span>
                        <span class="ml-4 font-bold text-2xl">Diskusikan Code-mu</span>
                    </div>
                    <div class="flex gap-x-4 md:gap-x-1 lg:gap-x-4 items-center ml-6">
                        <span class="absolute -left-5 px-4 py-2 bg-white rounded-xl font-semibold text-lg">3</span>
                        <span class="ml-4 font-bold text-2xl">Code-mu Terpecahkan</span>
                    </div>
                </div>
            </div>
            <button class="hover:bg-white hover:text-primary bg-primary transition-all duration-100 md:px-12 px-8 md:py-3 py-2 border-2 border-primary text-white rounded-xl self-center">Upload Isu Code! <span class="ml-3">></span></button>
        </div>
    </section>

    <section class="container mx-auto py-20 px-6 sm:px-8 lg:px-16 xl:px-24">
        <div class="text-center mx-5 mb-10 md:mb-20">
            <h3 class="text-primary text-xl font-semibold mb-2">USER'S VOICE</h3>
            <h2 class="md:text-4xl text-3xl font-semibold leading-snug">Jutaan Mahasiswa Telah Terbantu</h2>
        </div>
        <div class="flex gap-6 flex-col lg:flex-row">
            @foreach ($testimonials as $testimonial)
                <div class="bg-white shadow-primary py-10 px-6">
                    <div class="flex gap-x-1 mb-8">
                        @for ($i = 0; $i < 5; $i++)
                            <img class="w-8" src="https://em-content.zobj.net/thumbs/320/apple/325/star_2b50.png" alt="">
                        @endfor
                    </div>
                    <img class="w-4 mb-2" src="{{ asset('assets/testimonial/quote.svg') }}" alt="">
                    <p class="text-dark italic mb-8">{{ $testimonial["message"] }}</p>
                    <div class="flex items-center gap-x-4">
                        <img src="{{ $testimonial["photo"] }}" alt="{{ $testimonial["user"] }}">
                        <div class="flex flex-col gap-y-2">
                            <p class="text-dark text-2xl font-bold">{{ $testimonial["user"] }}</p>
                            <p class="text-gray">Mahasiswa {{ $testimonial["major"] }}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
</body>
@endsection