<!doctype html>
<html lang="id">

<head>
    @include('layouts.app')
    <style>
        input#email:not(:placeholder-shown),
        input#password:not(:placeholder-shown) {
            border: 2px solid #3482F6
        }
    </style>
    <title>Web E-Code | Login</title>
</head>

<body class="h-screen">
    <div class="grid grid-cols-1 md:grid-cols-2 h-full">
        <div class="container p-8 px-12 flex items-center justify-center">
            <form action="{{route('login.post')}}" method="POST" class="xl:w-9/12 lg:w-11/12 w-full">
                @csrf
                <a class="flex cursor-pointer" href="{{ route('landing') }}">
                    <img src="{{ asset('assets/ecode-logo.svg') }}" alt="Logo Ecode" class="rounded-lg" />
                    <div class="ml-4 align-middle text-2xl font-semibold">E-Code Web</div>
                </a>
                <h1 class="text-4xl font-bold text-black mt-14">Login</h1>
                <p class="text-[#A5A5A5] mt-2 mb-2">We’re glad to have you back! 😄</p>
                @if(session()->has("error"))
                    <div id="alert" class="toastr" style="display: none">
                        <span class="text-center w-full my-2 py-4 justify-center font-semibold flex items-center px-2.5 rounded-md text-medium font-medium bg-red-100 text-red-800">
                            <svg viewBox="0 0 24 24" width="18" height="18" stroke="currentColor" stroke-width="2.5" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><path d="M10.29 3.86L1.82 18a2 2 0 0 0 1.71 3h16.94a2 2 0 0 0 1.71-3L13.71 3.86a2 2 0 0 0-3.42 0z"></path><line x1="12" y1="9" x2="12" y2="13"></line><line x1="12" y1="17" x2="12.01" y2="17"></line></svg>
                            &nbsp;
                            {{session()->get("error")}}
                        </span>
                    </div>
                @endif
                <div>    
                    <label for="email" class="font-bold text-black">Email</label>
                    <input id="email" name="email"  type="email" value="{{old('email')}}" placeholder="Masukkan email Anda di sini..."
                        class="md:rounded-2xl font-mediun text-blue-500 rounded-xl border-[#D2D2D2] border-2 focus:outline-none focus:border focus:border-primary appearance-none block mt-3 w-full py-3 px-4">
                    @if ($errors->has("email"))
                        <p class="my-1 mx-2 text-xs font-semibold text-red-400">{{$errors->get("email")[0]}}</p>
                    @endif
                </div>
                <div class="mt-2 relative">
                    <label for="password" class="font-bold text-black">Password</label>
                    <div class="relative w-full">
                        <div class="absolute inset-y-0 right-0 pr-3 flex items-center">
                            <button class="text-gray-400 focus:outline-none focus:text-primary toggle-password"
                                type="button">
                                <img id="eye-auth" src="{{ asset('assets/authentication/eye-slash.svg') }}"
                                    alt="Toggle Password">
                            </button>
                        </div>
                        <input id="password"  name="password" type="password" placeholder="Masukan password Anda di sini.."
                            class="md:rounded-2xl font-mediun text-blue-500 rounded-xl border-[#D2D2D2] border-2 focus:outline-none focus:border focus:border-primary appearance-none block mt-3 w-full py-3 px-4">
                        @if ($errors->has("password"))
                            <p class="my-1 mx-2 text-xs font-semibold text-red-400">{{$errors->get("password")[0]}}</p>
                        @endif
                    </div>
                    <p class="text-primary font-bold cursor-pointer text-right w-full mt-2">Lupa password?</p>
                </div>
                <button
                    class="mt-8 py-3 px-6 border-white bg-primary md:rounded-2xl rounded-xl text-white w-full font-semibold">Login</button>
                {{-- <p class="text-center my-2 text-sm text-[#A5A5A5]">atau</p> --}}
                {{-- <button
                    class="py-3 px-6 border-[#D2D2D2] border bg-white md:rounded-2xl rounded-xl text-dark w-full font-semibold">
                    <div class="flex items-center justify-center">
                        <img class="h-5 mr-2 object-contain" src="{{ asset('assets/authentication/google.png') }}"
                            alt="Google">
                        <div class="h-auto font-bold">Login dengan Google</div>
                    </div>
                </button> --}}
                {{-- <div class="text-[#A5A5A5] text-center mt-10">Belum punya akun? <span
                        class="text-primary font-semibold cursor-pointer"><a
                            href="{{ route('register.page') }}">Register</a></span> dulu gasih 🗿</div> --}}
                <div class="text-[#A5A5A5] text-center mt-10">Mau balik lagi ke landing page? klik  <span
                    class="text-primary font-semibold cursor-pointer"><a
                        href="{{ route('landing') }}">Landing</a></span> yuk</div>
            </form>
        </div>
        <div
            class="hidden md:block bg-primary bg-[length:100%_70%] bg-[url('/assets/authentication/auth-bg.png')] bg-no-repeat bg-right-top">
            <div class="grid place-items-center h-full px-10">
                <img src="{{ asset('assets/authentication/auth-image.png') }}" alt="Authentication Image"
                    class="m-auto">
            </div>
        </div>
    </div>
</body>
<script src="{{asset('js/jquery3.6.js')}}"></script>
<script>
    $("#alert").delay(500).fadeIn();    
    setTimeout(() => {
        $(".toastr").delay(500).fadeOut();
    }, 5000);

    const textInvalid = (self, text) => {
        this.setCustomValidity(text);
    }
    
    const togglePassword = document.querySelector('.toggle-password');
    const passwordInput = document.querySelector('#password');
    const imgAuth = document.querySelector('#eye-auth');
    const eyeSlashPath = '/assets/authentication/eye-slash.svg';
    const eyePath = '/assets/authentication/eye.svg';

    togglePassword.addEventListener('click', () => {
        const type = passwordInput.getAttribute('type') === 'password' ? 'text' : 'password';
        imgAuth.setAttribute('src', passwordInput.getAttribute('type') === 'password' ? eyePath : eyeSlashPath);
        passwordInput.setAttribute('type', type);
    });
</script>

</html>
