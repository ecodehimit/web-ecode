<header>
    <div class="container mx-auto flex items-center justify-between px-6 sm:px-8 lg:px-16 xl:px-24">
        <div class="flex items-center gap-x-8">
            <a class="items-center" href="#">
                <img src="{{ asset('assets/ecode-logo.svg') }}" alt="Logo Ecode"
                    class="rounded-lg h-8 w-8 cursor-pointer" />
            </a>
            <nav class="nav items-center">
                <ul id="primary-navigation" data-visible="false"
                    class="primary-navigation flex gap-5 text-white sm:items-center sm:gap-0">
                    <li class="m-0 sm:mr-6 font-semibold"><a href="{{ route('login.page') }}">Home</a></li>
                    <li class="m-0 sm:mr-6"><a href="#">About</a></li>
                    <li class="m-0 sm:mr-6"><a href="#">Roadmap</a></li>
                    <li class="m-0 sm:mr-6"><a href="#">Webinars</a></li>
                    <li class="m-0 sm:mr-6"><a href="#">Verify Certificate</a></li>
                    <a class="sm:hidden py-2 px-6 rounded-xl bg-white text-dark font-semibold"
                        href="{{ route('login.page') }}">Login</a>
                </ul>
                <div class="nav-overlay"></div>
            </nav>
        </div>
        <button class="mobile-nav-toggle" aria-controls="primary-controls" aria-expanded="false">
            <span class="sr-only"></span>
        </button>
        <div class="h-full items-center hidden md:flex">
            <img src="{{ asset('assets/search.svg') }}" alt="search" class="w-5 h-5 mx-6 cursor-pointer">
            <a class="py-2 px-6 rounded-xl bg-white text-dark font-semibold" href="{{ route('login.page') }}">Login</a>
        </div>
    </div>
</header>
