<!doctype html>
<html lang="id">
  <head>
    @include('layouts.app')
    <title>Web E-Code</title>
  </head>

  @include('layouts.navbar')
  @yield("content")
  @include('layouts.footer')

  <script src="https://unpkg.com/@popperjs/core@2/dist/umd/popper.min.js"></script>
  <script src="https://unpkg.com/tippy.js@6/dist/tippy-bundle.umd.js"></script>
  <script src="{{ asset('js/app.js') }}"></script>
</html>