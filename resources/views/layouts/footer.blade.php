<footer class="bg-[#4A4A4A] text-white">
  <div class="container mx-auto py-16 px-6 sm:px-8 lg:px-16 xl:px-24">
      <div class="lg:flex lg:flex-row space-y-14 lg:space-y-0 lg:gap-y-12 lg:justify-between">
          <div class="flex flex-col gap-y-6 max-w-md">
              <div class="flex flex-row items-center gap-x-4">
                  <img class="w-16 rounded-xl" src="{{ asset('assets/ecode-logo.svg') }}" alt="Logo Ecode" class="rounded-lg h-8 w-8 cursor-pointer">
                  <h1 class="text-4xl font-semibold">E-Code</h1>
              </div>
              <div class="flex flex-col gap-y-4">
                  <p class="text-xl leading-relaxed sm:leading-relaxed">Himpunan Mahasiswa Teknik Informatika Politeknik Elektronika Negeri Surabaya</p>
                  <p class="">Jl. Raya ITS, Kec. Sukolilo, Surabaya 60111</p>
              </div>
          </div>
          <div class="flex flex-wrap gap-x-16">
              <div class="text-base mb-10">
                  <h4 class="font-semibold mb-6">TENTANG KAMI</h4>
                  <ul>
                      <li class="flex flex-col gap-y-3">
                          <a href="#">Sejarah</a>
                          <a href="#">Visi & Misi</a>
                          <a href="#">Prestasi</a>
                          <a href="#">Fasilitas</a>
                          <a href="#">Komunitas</a>
                      </li>
                  </ul>
              </div>
              <div class="text-base mb-10">
                  <h4 class="font-semibold mb-6">PRANALA LUAR</h4>
                  <ul>
                      <li class="flex flex-col gap-y-3">
                          <a href="#">Menristekdikti</a>
                          <a href="#">PENS</a>
                          <a href="#">DTIK PENS</a>
                          <a href="#">HIMIT PENS</a>
                      </li>
                  </ul>
              </div>
              <div class="text-base mb-10">
                  <h4 class="font-semibold mb-6">LEGAL</h4>
                  <ul>
                      <li class="flex flex-col gap-y-3">
                          <a href="#">Kebijakan Privasi</a>
                          <a href="#">Ketentuan dan Kondisi</a>
                      </li>
                  </ul>
              </div>
          </div>
      </div>
      <div class="flex flex-col md:flex-row md:justify-between pt-10 gap-y-6 border-t border-gray">
          <p class="text-center">© 2023 E-Code. Crafted with 💙 from Surabaya</p>
          <div class="flex flex-row gap-x-6 justify-center">
              <a target="_blank" href="">
                  <img src="{{ asset('assets/tech-icon/facebook.svg') }}">
              </a>
              <a target="_blank" href="https://www.instagram.com/ecode.himit">
                  <img src="{{ asset('assets/tech-icon/instagram.svg') }}">
              </a>
              <a target="_blank" href="https://www.linkedin.com/company/e-code-himit-pens/about/">
                  <img src="{{ asset('assets/tech-icon/linkedin.svg') }}">
              </a>
              <a target="_blank" href="#">
                  <img src="{{ asset('assets/tech-icon/github-light.svg') }}">
              </a>
              <a target="_blank" href="https://gitlab.com/ecodehimit">
                  <img src="{{ asset('assets/tech-icon/gitlab.svg') }}">
              </a>
          </div>
      </div>
  </div>
</footer>